# OpenML dataset: Mulcross

https://www.openml.org/d/40897

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

The data is generated from a synthetic data generator Mulcross (see Paper) and available. Mulcross generates a multi-variate normal distribution with a selectable number of anomaly clusters. In our experiments, the basic setting for Mulcross is as following: contamination ratio = 10% (number of anomalies over the total number of points), distance factor = 2 (distance between the center of normal cluster and anomaly clusters), and number of anomaly clusters = 2. This dataset is not the original dataset. The target variable &quot;Target&quot; is relabeled into &quot;Normal&quot; and &quot;Anomaly&quot;.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40897) of an [OpenML dataset](https://www.openml.org/d/40897). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40897/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40897/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40897/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

